<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;


// full name is "App\Http\Controllers\CandidatesController";
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index', compact('candidates','users','statuses'));
    }

    public function changeUser($cid,$uid = null){
        Gate::authorize('assign-user',Auth::user());
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        return back();
    }
    public function changeStatus($cid,$sid){
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status',$candidate)){
        $from = $candidate->status_id;
        if(!Status::allowed($sid,$from)) return back();
        $candidate->status_id = $sid;
        $candidate->save();
        }else{
            Session::flash('notallowed', 'You are  not allowed to chang the status of the user becuse you not the owner of the user ');
        }
        return back();
    }


    public function mycandidates(Request $request){

            $user_profile = Auth::id();
            $user = User::findorfail($user_profile);
            $candidates = $user->candidates;
            $users =  User:: all();
            $statuses = Status::all();
            return view('candidates.index',compact('candidates','users','statuses'));


    }

    public function sort($by){
        $candidates = Candidate::all()->sortBy($by);
        $users = User::all();
        $statuses = Status::all();
        return view('candidates.index',compact('candidates','users','statuses'));
        }

        public function candidateDetail ($id){
            $candidate = Candidate::findOrFail($id);
            $statuses = Status::all();
            return view('candidates.details',compact('candidate','statuses'));
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $candidate = new Candidate();
    //   $candidate ->name = $request ->name;
    //   $candidate ->email = $request ->email;
    $this-> validate($request,[
        'name' => 'required',
        'email' => 'required',
        'age'=> 'required'
    ]);
      $candidate ->create($request ->all());
      return redirect('candidates')->with('message','Candidate has been Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        return view('candidates.edit',compact('candidate'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $candidate = Candidate::findOrFail($id);
        $this-> validate($request,[
            'name' => 'required',
            'email' => 'required',
            'age'=> 'required'
        ]);
        $candidate->update($request->all());
        return redirect('candidates')->with('message','Candidate has been Updated');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates')->with('message','Candidate has been Removed');
    }
}
?>
