@extends('layouts.app')

@section('title','Candidate detail')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>

@endif

         @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif


    <h1> Candidate detail</h1>
    <table class="table table-dark">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Age</th> </th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th>
            </tr>
        <!-- the table data -->
            <tr>
            <td> {{$candidate->id}}</td>
            <td>{{$candidate->name}}</td>
            <td> {{$candidate->email}}</td>
            <td> {{$candidate->age}}</td>
            <td>
                    @if(@isset($candidate->user_id))
                    {{$candidate->owner->name}}
                     @else
                    Assign owner
                    @endif
            </td>
            <td> {{$candidate->status->name}}</td>
            <td> {{$candidate->created_at}}</td>
            <td> {{$candidate->updated_at}}</td>

        </tr>

    </table>
                           @csrf
                           <div class="col-md-6">
                            <select class="form-control" name="status_id">
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach(APP\Status::next($candidate->status_id) as $statuses)
                                <option value="{{ $statuses->id }}">
                                    {{ $statuses->name }}
                                </option>
                              @endforeach
                            </div>
                        </select>
                         <a class="btn btn-primary" href="{{route('candidate.changestatus',[$candidate->id,$statuses->id])}}"> Change Status </a></td>
                        </div>
                    </div>


@endsection
