@extends('layouts.app')

@section('title','Candidates')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>

@endif

         @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif
    <div><a class="badge badge-primary text-wrap" href="{{url('/candidates/create')}}">Add new candidate</a></div>

    <h1> List of candidates</h1>
    <table class="table table-dark">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th><a href ="{{url('/candidates/sort/age')}}">Age</a></th> </th><th>Owner</th><th>status</th> <th>Created</th><th>Updated</th>
            </tr>
        <!-- the table data -->
            @foreach($candidates as $candidate)
            <tr>
            <td> {{$candidate->id}}</td>
            <td><a href = "{{route('candidate.detail',[$candidate->id])}}"> {{$candidate->name}}</a></td>
            <td> {{$candidate->email}}</td>
            <td> {{$candidate->age}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(@isset($candidate->user_id))
                    {{$candidate->owner->name}}
                     @else
                    Assign owner
                    @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($users as $user)
                      <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
                      @endforeach
                    </div>
                  </div>
            </td>
            <td>
            <div class="dropdown">
                @if( null != APP\Status::next($candidate->status_id))
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(@isset($candidate->status_id))
                {{$candidate->status->name}}
                </button>
                @endif
                @else
                {{$candidate->status->name}}
                 @endif
                @if( null != APP\Status::next($candidate->status_id))
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(APP\Status::next($candidate->status_id) as $status)
                  <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->id])}}">{{$status->name}}</a>
                  @endforeach
                </div>
            </div>

               @endif
        </td>

            <td> {{$candidate->created_at}}</td>
            <td> {{$candidate->updated_at}}</td>
            <td> <a class="btn btn-primary" href="{{route('candidates.edit',$candidate->id)}}">Edit </a></td>
            <td> <a class="btn btn-primary" href="{{route('candidate.delete',$candidate->id)}}">Delete </a></td>
        </tr>
            @endforeach
    </table>
@endsection
