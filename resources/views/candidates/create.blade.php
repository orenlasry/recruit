@extends('layouts.app')

@section('title','Create Candidate')

@section('content')
    <body>
        <h1>create candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
            @csrf
            <div class="form-group" >
                <label for = "name">Candidate name</label>
                <input type = "text" class="form-control" name = "name">
            </div>
            <div class="form-group" >
                <label for = "email">Candidate email</label>
                <input type = "text" class="form-control" name = "email">
            </div>
            <div class="form-group" >
                <label for = "age">Candidate age</label>
                <input type = "text" class="form-control" name = "age">
            </div>
            <div class="form-group">
                <input type = "submit" class="form-control" name = "submit" value = "Create candidate">
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
             @endforeach
            </ul>
            </div>
        @endif
        </form>
    </body>
@endsection

