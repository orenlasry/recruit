@extends('layouts.app')

@section('title','Edit Candidate')

@section('content')
<h1>Edit Candidate <br> Name: {{$candidate->name}}</h1>


        <form method = "POST" action = "{{action('CandidatesController@update',$candidate->id)}}">
            @csrf
            @method('PATCH')


            <div class="form-group" >
                <label for = "name">Candidate name</label>
                <input type = "text" class="form-control" name = "name" value= {{$candidate->name}}>
            </div>
            <div class="form-group" >
                <label for = "email">Candidate email</label>
                <input type = "text" class="form-control" name = "email" value= {{$candidate->email}}>
            </div>
            <div class="form-group" >
                <label for = "age">Candidate email</label>
                <input type = "text" class="form-control" name = "age" value= {{$candidate->age}}>
            </div>
            <div>
                <input type = "submit" class="btn btn-outline-success" name = "submit" value = "Update candidate">
            </div>
            </div>
             @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                 @endforeach
                </ul>
                </div>
            @endif

        </form>

@endsection

