<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus');
Route::get('/mycandidates', 'CandidatesController@mycandidates')->name('candidates.mycandidates')->middleware('auth');
Route::get('/candidates/sort/{by}', 'CandidatesController@sort')->name('candidate.sort');
Route::get('/candidates/detail/{id}', 'CandidatesController@candidateDetail')->name('candidate.detail');
Route::post('users/create', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('users.create')->middleware('auth');
Route::get('users/add', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('users.add')->middleware('auth');


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
